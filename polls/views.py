from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404,render
from django.urls import reverse
from .models import Question, Choice

# Create your views here.
#/polls/요청시 
# latest_question_list에 Question의 모든데이터 select all 저장
# tempaltes - index.html select (latest_question_list전달)
def index(request) :
    #입력값 검증
    #business method 호출 : question목록 가져오기
    #                      select * from polls_question order by DECS pub_date
    latest_question_list = Question.objects.all().order_by('-pub_date')
    # print(type(latest_question_list))
    #일정범위에 data 저장
    context = {'latest_question_list':latest_question_list}
    #template select
    return render(request, 'polls/index.html', context)

#/polls/id/요청시 id에 해당하는 choice항목 출력하고
# vote button 출력
def detail(request, question_id) :
    #입력값 검증 : question_id
    #business method 호출 :question_id에 해당하는 Question 값 호출 
    
    #try :
    #   question = Question.objects.get(question_id)
    #except Question.DoesNotExist:
    #   raise Http404("Question does not exist")
    #shortcut : get_object_or_404 이용 
    question = get_object_or_404(Question, pk=question_id)
   
    #일정범위에 데이터 저장 - question
    #template select - polls/detail.html
    context = {'question': question}
    return render(request, "polls/detail.html", context) 

#/polls/id/vote 요청시 id의 vote add한 후
# 결과페이지로 redirect
def vote(request, question_id):
    #입력값 검증 : question_id 존재하지 않는 경우 404 error
    #business logic호출 : question_id의 Question객체 select
    question = get_object_or_404(Question, pk=question_id)

    #reqeust choice_id에 해당하는 vote조회 - 증가 - 수정
    #error :  question, error_message 저장, detail.html template select
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        context ={'question':question, 'error_message':"You didn't select a choice",}
        return render(request, 'polls/detail.html',context)        
    else :
        #select_choice vote 증가 - 수정
        selected_choice.votes += 1
        selected_choice.save()
        #redirect - result.html template select
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

#/polls/id/results/요청시 id에 해당하는 vote 결과 출력
def results(request, question_id):
    #입력값 검증 : question_id 존재하지 않는 경우 404 error
    #business logic호출 : question_id의 Question객체 select
    question = get_object_or_404(Question, pk=question_id)
    #일정 범위에 데이터 저장, template select
    context ={'question': question}
    return render(request, 'polls/results.html', context)