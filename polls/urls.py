from django.contrib import admin
from django.urls import path
from polls import views

app_name='polls'
urlpatterns = [
    path('',views.index, name='index'), #polls/  url polls:index
    path('<int:question_id>/',views.detail, name='detail'),#polls/2  polls:detail
    path('<int:question_id>/results',views.results, name='results'),#polls/2/results polls:results
    path('<int:question_id>/vote',views.vote, name='vote'),#polls/2/vote  polls:vote
]
